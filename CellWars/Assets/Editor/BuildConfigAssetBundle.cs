﻿using UnityEngine;
using UnityEditor;

public class BuildConfigAssetBundle : MonoBehaviour
{
	[MenuItem( "BuildConfig/Build Standalone Config Asset Bundles" )]
	static void BuildStandaloneConfig( )
	{
		// Create the array of bundle build details.
		AssetBundleBuild[] buildMap = new AssetBundleBuild[1];

		buildMap[0].assetBundleName = "config";

		string[] configAssets = new string[4];
		configAssets[0] = "Assets/Documents/UnityConfig/chapterConfig.asset";
		configAssets[1] = "Assets/Documents/UnityConfig/cellConfig.asset";
		configAssets[2] = "Assets/Documents/UnityConfig/virsColor.asset";
		configAssets[3] = "Assets/Documents/UnityConfig/virusConfig.asset";
		buildMap[0].assetNames = configAssets;

		BuildPipeline.BuildAssetBundles( Application.streamingAssetsPath + "/Documents", buildMap, BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows );
	}

	[MenuItem( "BuildConfig/Build IOS Config Asset Bundles" )]
	static void BuildIOSConfig( )
	{
		// Create the array of bundle build details.
		AssetBundleBuild[] buildMap = new AssetBundleBuild[1];

		buildMap[0].assetBundleName = "config";

		string[] configAssets = new string[4];
		configAssets[0] = "Assets/Documents/UnityConfig/chapterConfig.asset";
		configAssets[1] = "Assets/Documents/UnityConfig/cellConfig.asset";
		configAssets[2] = "Assets/Documents/UnityConfig/virsColor.asset";
		configAssets[3] = "Assets/Documents/UnityConfig/virusConfig.asset";
		buildMap[0].assetNames = configAssets;

		BuildPipeline.BuildAssetBundles( Application.streamingAssetsPath + "/Documents", buildMap, BuildAssetBundleOptions.None, BuildTarget.iOS );
	}
}