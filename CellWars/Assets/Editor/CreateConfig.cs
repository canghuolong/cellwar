﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;
using System.Reflection;

public class CreateConfig : Editor
{
	const string SCRIPTTEMPLATE = "using UnityEngine; public class {0} { {1} }";

	[MenuItem("Assets/CreateConfigs")]
	static void CreateConfigs()
	{
		string[] configList = Directory.GetFiles(PathManager.getConfigPath, "*.json");
		SZDebug.Log(configList.Length);
		for (int i = 0; i < configList.Length; i++) {
			string template = SCRIPTTEMPLATE;

			string str = configList[i];
			Match m = Regex.Match(str, @"\Config/([\s\S]*?)\.");//这边*在正则是特殊字符 所以前面都加一个“\” 
			if (m.Success) {
				str = m.Result("$1");
			} else {
				str = "fail";
				SZDebug.LogError("Error filename error");
				return;
			}
			string className = SetStringFirstUpper(str);	
			template = template.Replace("{0}", className + ": ScriptableObject");

			Dictionary<string, object> tempDic = (Dictionary<string, object>)JsonReader.GetConfig(configList[i]).mData;
			foreach (KeyValuePair<string, object> key in tempDic) {
//				这个表的字典				
//				SZDebug.Log("key:" + key.Key + ":::" + key.Value, SZDebug.Color.YELLOW);
				string classContent = "";
				Dictionary<string, object> tDic = (Dictionary<string, object>)key.Value;
				foreach (KeyValuePair<string, object> k in tDic) {
//					每一行的列表字典					
					SZDebug.Log("key" + k.Key + "---value" + k.Value);
					float tempValue = -999f;

					string tempStr = "public ";
					if (float.TryParse(k.Value.ToString(), out tempValue)) {
//						SZDebug.Log(tempValue, SZDebug.Color.YELLOW) ;	
						tempStr += "float ";
					} else {
						tempStr += "string ";
					}
					tempStr += k.Key.ToString() + ";";
					classContent += tempStr;
				}
				template = template.Replace("{1}", classContent);
				break;
			}	
			SZDebug.Log(template, SZDebug.Color.WHITE);
			File.WriteAllText(Application.dataPath + "/Scripts/Config/" + className + ".cs", template, System.Text.Encoding.UTF8);
		}
	}

	[MenuItem("Assets/SetConfigValue")]
	public static void SetConfigValue()
	{
		Dictionary<string, object> tempDic = (Dictionary<string, object>)JsonReader.GetConfig(PathManager.getConfigPath + "chapterData.json").mData;
		ChapterConfig tempChapter = ScriptableObject.CreateInstance<ChapterConfig>();
		tempChapter.chapterList = new List<ChapterData>();
		foreach (KeyValuePair<string, object > key in tempDic) {
			int dicKey = int.Parse(key.Key);
			ChapterData tempCd = new ChapterData();
			tempCd.cellData = new List<string>();
			Dictionary<string, object> dic = (Dictionary<string, object>)key.Value;
			foreach (KeyValuePair<string, object> k in dic) {
				if (k.Key == "ID") {
					tempCd.ID = int.Parse(k.Value.ToString());
				} else if (k.Key == "dropId") {
					tempCd.dropId = int.Parse(k.Value.ToString());
				} else if (k.Key == "cell01Data" || k.Key == "cell02Data" || k.Key == "cell03Data" || k.Key == "cell04Data" || k.Key == "cell05Data" || k.Key == "cell06Data" || k.Key == "cell07Data" || k.Key == "cell08Data" ||
				          k.Key == "cell09Data" || k.Key == "cell10Data" || k.Key == "cell11Data" || k.Key == "cell12Data" || k.Key == "cell13Data" || k.Key == "cell14Data" || k.Key == "cell15Data" || k.Key == "cell16Data" ||
				          k.Key == "cell17Data" || k.Key == "cell18Data" || k.Key == "cell19Data" || k.Key == "cell20Data" || k.Key == "cell21Data" || k.Key == "cell22Data" || k.Key == "cell23Data" || k.Key == "cell24Data") {
					if (string.IsNullOrEmpty(k.Value.ToString())) {
						SZDebug.Log("There is no cell infomation!");
					} else {
						string tempStr = k.Value.ToString();
						tempCd.cellData.Add(tempStr);	
					}
				}
			}
			Debug.Log(dicKey);
			tempChapter.chapterList.Add(tempCd);
		}
		SaveScriptableObject(tempChapter, "Assets/Documents/UnityConfig/chapterConfig.asset");
		Debug.Log(tempChapter.chapterList.Count);
	
		tempDic = (Dictionary<string, object>)JsonReader.GetConfig(PathManager.getConfigPath + "cellsType.json").mData;
		CellConfig tempCell = ScriptableObject.CreateInstance<CellConfig>(); 
		tempCell.cellList = new List<CellsType>();
		foreach (KeyValuePair<string, object> key in tempDic) {
			CellsType cellType = new CellsType();
			Dictionary<string, object> dic = (Dictionary<string, object>)key.Value;
			foreach (KeyValuePair<string, object> k in dic) {
				AddValue<CellsType>(cellType, k.Key, k.Value);
			}
			tempCell.cellList.Add(cellType);
		}
		SaveScriptableObject(tempCell, "Assets/Documents/UnityConfig/cellConfig.asset");


		tempDic = (Dictionary<string, object>)JsonReader.GetConfig(PathManager.getConfigPath + "virsAblity.json").mData;
		VirusConfig tempVirus =  ScriptableObject.CreateInstance<VirusConfig>(); 
		tempVirus.virusList = new List<VirsAblity>();
		foreach(KeyValuePair<string, object> key in tempDic) {
			VirsAblity virsAblity = new VirsAblity();
			Dictionary<string, object> dic = (Dictionary<string,object>)key.Value;
			foreach(KeyValuePair<string, object> k in dic) {
				AddValue<VirsAblity>(virsAblity, k.Key, k.Value);
			}
			tempVirus.virusList.Add(virsAblity);
		}
		SaveScriptableObject(tempVirus, "Assets/Documents/UnityConfig/virusConfig.asset");

		tempDic = (Dictionary<string, object>)JsonReader.GetConfig(PathManager.getConfigPath + "virsColor.json").mData;
		ColorConfig tempColor =  ScriptableObject.CreateInstance<ColorConfig>(); 
		tempColor.colorConfigList = new List<VirsColor>();
		foreach(KeyValuePair<string, object> key in tempDic) {
			VirsColor virsColor = new VirsColor();
			Dictionary<string, object> dic = (Dictionary<string,object>)key.Value;
			foreach(KeyValuePair<string, object> k in dic) {
				AddValue<VirsColor>(virsColor, k.Key, k.Value);
			}
			tempColor.colorConfigList.Add(virsColor);
		}
		SaveScriptableObject(tempColor, "Assets/Documents/UnityConfig/virsColor.asset");
	}

	static T AddValue<T>(T field, string fieldName, object fieldValue) {
		FieldInfo[] fieldInfo = field.GetType().GetFields();
		for (int i = 0; i < fieldInfo.Length; i++) {
			if (fieldInfo[i].Name.Equals(fieldName)) {
				if (fieldInfo[i].FieldType.Equals(typeof(System.Single)) || fieldInfo[i].FieldType.Equals(typeof(System.Double))) {
					fieldInfo[i].SetValue(field, float.Parse(fieldValue.ToString()));							
				} else if (fieldInfo[i].FieldType == typeof(System.Int64) || fieldInfo[i].FieldType == typeof(System.Int32)) {
					fieldInfo[i].SetValue(field, int.Parse(fieldValue.ToString()));
				} else {
					fieldInfo[i].SetValue(field, fieldValue.ToString());
				}
			}
		}

		return field;
	}

	static void SaveScriptableObject(ScriptableObject scriptObject, string path) {
		Debug.Log("path:" + path);
		AssetDatabase.CreateAsset(scriptObject, path);
		AssetDatabase.SaveAssets();
	}

	static string SetStringFirstUpper(string origionStr)
	{
		char tempChar = char.ToUpper(origionStr[0]);
		string newStr = tempChar + origionStr.Substring(1);
		return newStr;
	}
}
