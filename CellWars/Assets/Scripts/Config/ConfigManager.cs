﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;	

public class ConfigManager {
	/// <summary>
	/// 章节配置路径
	/// </summary>
	public const string CHAPTERCONFIGPATH = "chapterConfig";
	/// <summary>
	/// 细胞配置路径
	/// </summary>
	public const string CELLCONFIGPATH = "cellConfig";
	/// <summary>
	/// 病毒配置路径
	/// </summary>
	public const string VIRUSCONFIG = "virusConfig";
	/// <summary>
	/// 病毒颜色配置路径
	/// </summary>
	public const string VIRUSCOLORCONFIG = "virsColor";

	public AssetBundle assetBundle;

	ConfigManager(){
		SZDebug.Log("configmanager init", SZDebug.Color.GREEN);
		var files = Directory.GetFiles(Application.streamingAssetsPath + "/Documents");
		SZDebug.Log("File counts:" +  files.Length, SZDebug.Color.WHITE);
		assetBundle = AssetBundle.LoadFromFile(Application.streamingAssetsPath + "/Documents/config");
	}
	private static ConfigManager instance;
	public static ConfigManager getInstance {
		get {
			if(instance == null) {
				instance = new ConfigManager() ;
			}
			return instance;
		}
	}

	private Dictionary<int, ChapterData> chapterConfigdic;
	/// <summary>
	/// 关卡配置表
	/// </summary>
	public Dictionary<int, ChapterData> getChapterConfigDic {
		get {
			if(chapterConfigdic == null)	{
				chapterConfigdic = new Dictionary<int, ChapterData>();
				ChapterConfig tempChapter = assetBundle.LoadAsset<ChapterConfig>(CHAPTERCONFIGPATH);
				for(int i = 0; i < tempChapter.chapterList.Count; i ++) {
					chapterConfigdic.Add(tempChapter.chapterList[i].ID, tempChapter.chapterList[i]);
				}
			}
			return chapterConfigdic;
		}
	}

	private Dictionary<int, CellsType> cellConfigDic;
	/// <summary>
	/// 细胞配置表
	/// </summary>
	public Dictionary<int, CellsType> getCellConfigDic {
		get {
			if(cellConfigDic == null)	{
				cellConfigDic = new Dictionary<int, CellsType>();
				CellConfig tempCell = assetBundle.LoadAsset<CellConfig>(CELLCONFIGPATH); 
				for(int i = 0; i < tempCell.cellList.Count; i ++) {
					cellConfigDic.Add(tempCell.cellList[i].ID, tempCell.cellList[i]);
				}
			}
			return cellConfigDic;
		}
	}

	private Dictionary<int, VirsAblity> virusConfigDic;
	/// <summary>
	/// 病毒配置表
	/// </summary>
	public Dictionary<int, VirsAblity> getVirusConfigDic {
		get {
			if(virusConfigDic == null)	{
				virusConfigDic = new Dictionary<int, VirsAblity>();
				VirusConfig tempVirus = assetBundle.LoadAsset<VirusConfig>(VIRUSCONFIG);
				for(int i = 0; i < tempVirus.virusList.Count; i ++) {
					virusConfigDic.Add(tempVirus.virusList[i].ID, tempVirus.virusList[i]);
				}
			}
			return virusConfigDic;
		}
	}

	private Dictionary<int, VirsColor> virusColorConfigDic;
	public Dictionary<int, VirsColor> getVirusColorDic {
		get {
			if(virusColorConfigDic == null) {
				virusColorConfigDic = new Dictionary<int, VirsColor>();
				ColorConfig tempColor = assetBundle.LoadAsset<ColorConfig>(VIRUSCOLORCONFIG);
				for(int i = 0; i < tempColor.colorConfigList.Count; i ++) {
					virusColorConfigDic.Add(tempColor.colorConfigList[i].ID, tempColor.colorConfigList[i]);
				}
			}
			return virusColorConfigDic;
		}
	}



	public void ClearConfigManager() {
		assetBundle.Unload(true);
		assetBundle = null;
		instance = null;
	}
}
