﻿using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 细胞Asset配置
/// </summary>
public class CellConfig : ScriptableObject {
	public List<CellsType> cellList;
}
/// <summary>
/// 细胞原始配置
/// </summary>
[System.Serializable]
public class CellsType 
{
	public int ID;
	public int type;
	public float radius;
	public int content;
	public float atkPlus;
	public float defPlus;
	public float spdPlus;
	public float agiPlus;
	public float repPlus;
}