﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// 病毒asset 配置类
/// </summary>
public class VirusConfig : ScriptableObject{
	/// <summary>
	/// 病毒数组
	/// </summary>
	public List<VirsAblity> virusList;

	void OnDestroy() {
		Debug.LogError("who destroy me?");

	}
}
/// <summary>
/// 病毒原始配置
/// </summary>
[System.Serializable]
 public class VirsAblity
{
	public int ID;
	/// <summary>
	/// 初始数量
	/// </summary>
	public float virsNum;
	/// <summary>
	/// 0-中立 1-自己 2-8其他
	/// </summary>
	public int force;
	public int atk;
	public int def;
	public int spd;
	/// <summary>
	/// 耐压缩
	/// </summary>
	public int agi;
	/// <summary>
	/// 再生
	/// </summary>
	public int rep;
	public int AI;
	/// <summary>
	/// 颜色
	/// </summary>
	public int color;
}