﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 颜色Asset配置
/// </summary>
public class ColorConfig : ScriptableObject {
	/// <summary>
	/// 颜色配置列表
	/// </summary>
	public List<VirsColor> colorConfigList;
}
/// <summary>
/// 病毒颜色原始配置类
/// </summary>
[System.Serializable]
public class VirsColor
{
	/// <summary>
	/// 颜色ID
	/// </summary>
	public int ID;
	/// <summary>
	/// 红
	/// </summary>
	public int red ;
	/// <summary>
	/// 绿
	/// </summary>
	public int green;
	/// <summary>
	/// 蓝
	/// </summary>
	public int blue ;
}
