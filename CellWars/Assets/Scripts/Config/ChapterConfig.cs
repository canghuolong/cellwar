﻿using UnityEngine;
using System.Collections.Generic;
/// <summary>
/// 用来存储关卡的Asset配置类
/// </summary>
public class ChapterConfig : ScriptableObject{
	public List<ChapterData> chapterList;
}
[System.Serializable]
public class ChapterData{
	public int ID;
	public int dropId;
	public List<string> cellData;
}