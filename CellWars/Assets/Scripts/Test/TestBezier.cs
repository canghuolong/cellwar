﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBezier : MonoBehaviour {
	public Transform[] points;
	public Transform[] controlPoits;

	LTBezierPath br;

	public Transform ss;
	float aaa;
	void OnEnable() {
		
	}

	void OnDrawGizmos() {
//		if(Application.isPlaying) {
			br = new LTBezierPath(new Vector3[]{points[0].position, controlPoits[0].position, controlPoits[1].position, points[1].position,
			points[1].position, controlPoits[2].position, controlPoits[3].position, points[2].position});
		


			br.gizmoDraw();	
//		}
	}

	void Update() {
		if(br == null) {
			return;
		}
		br.place(ss, aaa);
		aaa += Time.deltaTime * 0.5f;
		if(aaa > 1) {
			aaa = 0;
		}
	}
}
