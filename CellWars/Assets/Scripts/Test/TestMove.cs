﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMove : MonoBehaviour {
	public GameObject moveHH;
	public Transform targetPoint;
	public Transform startPoint;
	float process;

	public Transform bg;



	public Transform points;
	LTSpline spline;

	// Use this for initialization
	void Start () {
		Vector3[] splinevec = new Vector3[points.childCount];
		for(int i = 0; i < points.childCount; i ++) {
			splinevec[i] = points.GetChild(i).position;
		}
		spline = new LTSpline(splinevec);
		processhaha = Random.Range(0, 0.5f);
	}
	float processhaha = 0;
	float startPro = 0;
	// Update is called once per frame
	void Update () {

		if(Input.GetKeyDown(KeyCode.A)) {
			
			LeanTween.value(gameObject, startPro, processhaha, 1).setOnUpdate(delegate(float obj) {
				process = obj	;
//				transform.LookAt(targetPoint);
				spline.place(gameObject.transform, process);
			}).setEaseOutQuad();
			LeanTween.scaleY(bg.gameObject, 0.1f, 0.35f);
			LeanTween.scaleX(bg.gameObject, 0.5f, 0.35f);

			LeanTween.scaleY(bg.gameObject, 0.3f, 0.35f).setDelay(0.35f);
			LeanTween.scaleX(bg.gameObject, 0.3f, 0.35f).setDelay(0.35f);
			if(processhaha >= 1){
				startPro = 0;
				processhaha = Random.Range(0, 0.5f);
			} 
			startPro = processhaha;
			processhaha += Random.Range(0, 0.5f);
			if(processhaha > 1) {
				processhaha = 1;
			}
		}
	}

	void OnDrawGizmos() {
		Vector3[] splinevec = new Vector3[points.childCount];
		for(int i = 0; i < points.childCount; i ++) {
			splinevec[i] = points.GetChild(i).position;
		}
		spline = new LTSpline(splinevec);
		spline.drawGizmo(Color.blue);
	}
}
