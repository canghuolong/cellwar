﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPath : MonoBehaviour {
	public GameObject cell;
	public GameObject[] celll;

	public GameObject startCircle;
	Vector3 startPosition{
		get {
			return startCircle.transform.position;
		}
	}
	float startRadius {
		get {
			return startCircle.transform.localScale.x * 0.5f;
		}
	}
	public GameObject endCircle;
	Vector3 endPosition {
		get {
			return endCircle.transform.position;
		}
	}
	float endRadius {
		get {
			return endCircle.transform.localScale.x * 0.5f;
		}
	}

	public GameObject[] circleList ;
	public List<CircleStrust> circleList2 = new List<CircleStrust>();

	Vector2 point1;

	Vector2[] startPoints;
	Vector2[] endPoints;

	LTSpline[] splineArray;

	public int randomCounts = 1;
	// Use this for initialization
	void Start () {
		startPoints = GeometryTool.GenerateRandomPoint(startPosition, startRadius, randomCounts);
		endPoints = GeometryTool.GenerateRandomPoint(endPosition, endRadius, randomCounts);

		splineArray = new LTSpline[randomCounts];

		for(int i = 0; i < circleList.Length; i ++) {
			circleList2.Add(new CircleStrust(circleList[i]));
		}
		for(int i = 0; i < randomCounts; i ++) {
			GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
			sphere.transform.localScale = Vector3.one * 0.2f;
			sphere.transform.position = new Vector3(startPoints[i].x, startPoints[i].y, -5);
			sphere.GetComponent<MeshRenderer>().material.color = Color.red;	

			sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
			sphere.transform.localScale = Vector3.one * 0.2f;
			sphere.transform.position =  new Vector3(endPoints[i].x, endPoints[i].y, -5);
			sphere.GetComponent<MeshRenderer>().material.color = Color.magenta;	
		}

		celll = new GameObject[randomCounts];
		for(int i = 0; i < randomCounts; i ++) {
			celll[i] = Instantiate(cell);
		}
	}

	List<GameObject> bbb = new List<GameObject>();

	void OnDrawGizmos() {
		if(Application.isPlaying) {

			for(int i = 0; i < randomCounts; i ++) {
				splineArray[i] = new SZSpline(circleList2, startPoints[i], endPoints[i]).getSpline;
				//				for(int j = 0; j < splineArray[i].pts.Length; j ++) {
				//					GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				//					sphere.name = "------";
				//					sphere.transform.localScale = Vector3.one * 0.1f;
				//					sphere.transform.position =  splineArray[i].pts[j];
				//					sphere.GetComponent<MeshRenderer>().material.color = Color.red;	
				//				}

//
//				LTSpline ttt = splineArray[i];
//				GameObject tempGo = celll[i];
//				LeanTween.value(tempGo, 0, 1, 2).setOnUpdate(delegate(float obj) {
//					ttt.place(tempGo.transform,obj);
//				}).setDelay(Random.Range(0, 1f));
			}

			for(int i = 0; i < randomCounts; i ++) {
				Gizmos.color = Color.blue;
				Gizmos.DrawLine(startPoints[i], endPoints[i]);

				if(splineArray.Length > 0) {
					if(splineArray[i] != null) {

							for(int j = bbb.Count; j < splineArray[i].pts.Length; j ++) {
								GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
							sphere.name = "Key Point";
								sphere.transform.localScale = Vector3.one * 0.1f;
								bbb.Add(sphere);
							}


						for(int j = 0; j < splineArray[i].pts.Length; j ++) {
							bbb[j].name = "KeyPoint:" + j;
							bbb[j].transform.position = splineArray[i].pts[j];
						}

						splineArray[i].drawGizmo(Color.yellow);		
					}
				}
			}
		}
	}

	List<Vector2> pointsList = new List<Vector2>();
	Vector2 xiangjiaoPoint = Vector2.zero ;
	void FindWay(Vector2 startP, Vector2 endP) {
		
		Vector2 finalPoint = new Vector2(-999, -999);
		Vector2 circlePoint = new Vector2(-999, -999);
		int iddd = -1;
		for(int i = 0; i < circleList.Length; i ++) {
			if(GeometryTool.LineInterCircle(startP, endP, GeometryTool.ConvertVec3ToVec2(circleList[i].transform.position)
				,circleList[i].transform.localScale.x * 0.5f,ref xiangjiaoPoint)) {
				if(Vector3.Distance(startP, xiangjiaoPoint) < Vector3.Distance(startP, finalPoint)) {
					finalPoint = xiangjiaoPoint;
					iddd = i;
				}
			}
		}
		if(finalPoint.x == -999) {
			Debug.Log("spline count:" + pointsList.Count + "VecStart:" + startP + "VecEnd:" + endP );	
		} else {
			pointsList.Add(finalPoint);
			circlePoint = GeometryTool.GenerateOutCirlePoint(GeometryTool.ConverVec2ToVec3(startP),
				GeometryTool.ConverVec2ToVec3(endP), circleList[iddd].transform.position, 
				circleList[iddd].transform.localScale.x * 0.5f);
			pointsList.Add(circlePoint);
			FindWay(circlePoint, endP);
		}

	}

	float startValue = 0;
	int i = 0;
	void Move() {
		
		LeanTween.value(gameObject, startValue, 1, 1).setOnUpdate(delegate(float obj) {
			splineArray[0].place(transform, obj);
		}).setOnComplete(delegate() {
			Debug.LogError("TweenCounts:" + LeanTween.startSearch + "---" + i);
			startValue += 0.2f;
			if(i < 5) {
				Move();
			}
			i ++;
		});
	}

	// Update is called once per frame
	void Update () {
		for(int i = 0; i < circleList.Length; i ++) {
			circleList2[i] = new CircleStrust(circleList[i]);
		}
		if(Input.GetKeyDown(KeyCode.R)) {
			for(int i = 0; i < randomCounts; i ++) {
				VirusManager c = new VirusManager( new VirusAttribute(),new CellManager(), new CellManager());
					c.CellMove(splineArray[i]);	
			}
		}

		if(Input.GetKeyDown(KeyCode.S)) {
			bool isXJ = GeometryTool.LineInterCircle(GeometryTool.ConvertVec3ToVec2(startCircle.transform.position),
				GeometryTool.ConvertVec3ToVec2(endCircle.transform.position),
				GeometryTool.ConvertVec3ToVec2(circleList[0].transform.position),
				circleList[0].transform.localScale.x * 0.5f, ref point1);
			if(isXJ) {
				GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				sphere.transform.localScale = Vector3.one * 0.1f;
				sphere.transform.position =  point1;
				sphere.GetComponent<MeshRenderer>().material.color = Color.red;	
			}

		}

		if(Input.GetKeyDown(KeyCode.D)) {
			Vector3 temp = GeometryTool.GenerateOutCirlePoint(startCircle.transform.position, endCircle.transform.position, circleList[0].transform.position, circleList[0].transform.localScale.x * 0.5f);
			GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
			sphere.transform.localScale = Vector3.one * 0.1f;
			sphere.transform.position =  temp;
			sphere.GetComponent<MeshRenderer>().material.color = Color.red;	
		}

		if(Input.GetKeyDown(KeyCode.Space)) {
							LTSpline ttt = splineArray[0];
							GameObject tempGo = celll[0];
			for(int i = 0; i < ttt.pts.Length; i ++) {
				GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				sphere.GetComponent<MeshRenderer>().material.color = Color.green;	
				sphere.transform.localScale = Vector3.one * 0.1f;
				sphere.transform.position = ttt.pts[i];
			}
							LeanTween.value(tempGo, 0, 1, 5).setOnUpdate(delegate(float obj) {
//								ttt.place(tempGo.transform,obj);
				tempGo.transform.position = ttt.point(obj);

							});
		}
	}
}