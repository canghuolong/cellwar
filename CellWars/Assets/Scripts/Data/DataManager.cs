﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager {
	static DataManager instance;
	public static DataManager getInstance {
		get {
			if(instance == null) {
				instance = new DataManager();
				SZDebug.Log("data manager init");
			}
			return instance;
		}
	}

	public int currentChapterId = 10000001;

	public PlayerData playerData  = new PlayerData();
	public VirusAttribute currentVirus;

	public void GetPlayerData(){
//		playerData = new PlayerData();
//		playerData.playerViruses = new VirusAttribute[1];
//		playerData.playerViruses[0] = new VirusAttribute();
	}


	/// <summary>
	/// 取得关卡信息
	/// </summary>
	/// <returns>关卡里细胞数据</returns>
	/// <param name="chapterID">关卡ID</param>
	public ChapterData GetChapterData(int chapterID) {
		ChapterData chapterData = ConfigManager.getInstance.getChapterConfigDic[chapterID];
		return chapterData;
	}


}
