﻿using System.Collections;
using System.Collections.Generic;

public class PlayerData {
	public string playerId;
	public int gold;
	public int diamond;

	public VirusAttribute[] playerViruses;
}
