﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellCommon  {
	//颜色路径
	const string COLORPATH = "ConstConfig/CellColor";
	static ColorConst color;
	//传入类型得到颜色
	public static Color GetColor(int type) {
		if(color == null) {
			color  = Resources.Load<ColorConst>(COLORPATH);
		}
		switch(type) {
			case 0:
				return color.whiteCell;
			case 1:
				return color.blueCell;
			case 2:
				return color.blackCell;
			case 3:
				return color.redCell;
			case 4:
				return color.yellowCell;
			case 5:
				return color.purpleCell;
			case 6:
				return color.greenCell;
			case 7:
				return color.indigoCell;
			case 8:
				return color.grayCell;
			default:
				return color.whiteCell;
		}
	}
}
