﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 病毒属性
/// </summary>
public class VirusAttribute  {

	public int ID;
	public float virsNum;
	/// <summary>
	/// 0-中立 1-自己 2-8其他
	/// </summary>
	public int force;
	public int atk;
	public int def;
	public int spd;
	/// <summary>
	/// 耐压缩
	/// </summary>
	public int agi;
	/// <summary>
	/// 再生
	/// </summary>
	public int rep;
	public int AI;
	/// <summary>
	/// 颜色
	/// </summary>
	public int color;

	public VirusAttribute(){}

	public VirusAttribute(int atk, int def, int agi, int rep, int spd, int color) {
		this.atk = atk;
		this.def = def;
		this.agi = agi;
		this.rep = rep;
		this.spd = spd;
		this.color = color;
	}

	public  VirusAttribute SetOtherAttribute(VirsAblity virsAblity) {
		this.ID = virsAblity.ID;
		this.virsNum = virsAblity.virsNum;
		this.force = virsAblity.force;
		this.AI = virsAblity.AI;
		return this;
	}

	public VirusAttribute(VirsAblity virsAblity) {
		this.ID = virsAblity.ID;
		this.virsNum = virsAblity.virsNum;
		this.force = virsAblity.force;
		this.atk = virsAblity.atk;
		this.def = virsAblity.def;
		this.spd = virsAblity.spd;
		this.agi = virsAblity.agi;
		this.rep = virsAblity.rep;
		this.AI = virsAblity.AI;
		this.color = virsAblity.color;
	}
}
