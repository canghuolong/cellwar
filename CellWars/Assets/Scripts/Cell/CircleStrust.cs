﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//圆圈结构体
[SerializeField]
public class CircleStrust {
	public Vector2 centre;
	public float radius;
	public GameObject circleGo;
	public CircleStrust(Vector3 mcentre, float mradius) {
		centre = mcentre;
		radius = mradius;
	}

	public CircleStrust(Vector2 mcentre, float mradius) {
		centre = mcentre;
		radius = mradius;
	}

	public CircleStrust(GameObject mcircleGo) {
		circleGo = mcircleGo;
		centre = circleGo.transform.position;
		radius = circleGo.transform.localScale.x * 0.5f;
	}
}
