﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 细胞属性
/// </summary>
public class CellAttribute  {
	public int ID;
	public int type;
	/// <summary>
	/// 病毒数量
	/// </summary>
	public float virusNum;
	public float radius;
	/// <summary>
	/// 容量上限
	/// </summary>
	public int content;
	public float atkPlus;
	public float defPlus;
	public float speedPlus;
	public float agiPlus;
	public float repPlus;
	public Vector3 position;
	/// <summary>
	/// 细胞挂载的病毒信息
	/// </summary>
	public VirusAttribute virsAttribute;


	public CellAttribute(CellsType cellType, Vector3 pos, VirusAttribute virs) {
		this.ID = cellType.ID;
		this.type = cellType.type;
		this.virusNum = virs.virsNum;
		this.radius = cellType.radius;
		this.content = cellType.content + Mathf.FloorToInt((cellType.agiPlus * cellType.content) / 10);
		this.atkPlus = cellType.atkPlus;
		this.defPlus = cellType.defPlus;
		this.speedPlus = cellType.spdPlus;
		this.agiPlus = cellType.agiPlus;
		this.repPlus = cellType.repPlus;
		this.position = pos;

		this.virsAttribute = virs;
	}

}
