﻿//#define CELLTEST

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 细胞管理器
/// </summary>
public class CellManager {

	/// <summary>
	/// 阵营枚举
	/// </summary>
	public enum Force{
		/// <summary>
		/// 己方
		/// </summary>
		SELF,
		/// <summary>
		/// 敌方
		/// </summary>
		ENEMY,
		/// <summary>
		/// 中立neutral
		/// </summary>
		NEUTRAL
	}
	public Force getForce {
		get {
			if(mCellAttribute.virsAttribute.force == BattleConstDefine.NEUTRAL) {
				return Force.NEUTRAL;
			} else if(mCellAttribute.virsAttribute.force == BattleConstDefine.SELFFORCE) {
				return Force.SELF;
			} else {
				return Force.ENEMY;
			}
		}
	}
	/// <summary>
	/// 2个细胞生成时间
	/// </summary>
	const float GENERATETIME = 0.3f;
	/// <summary>
	/// 时间增量
	/// </summary>
	const float TIMEINCREMENT = 0.2f;


	/// <summary>
	/// 细胞营根目录所在的子物体id索引
	/// </summary>
	const int CELLBGINDEX = 1;
	//细胞营本体
	public GameObject cellObj;
	//细胞营位置
	public Vector3 cellPosition {
		get {
			return cellObj.transform.localPosition;
		}
	}
	//细胞营文本
	public UnityEngine.UI.Text text;
	//细胞营背景
	public Transform cellBackground;
	//细胞属性
	public CellAttribute mCellAttribute;
	//细胞半径
	public float radius{
		get {
			return cellBackground.localScale.x * 0.5f;
		}
	}
	/// <summary>
	/// 所有细胞数组
	/// </summary>
	public List<CircleStrust> circleList;

	/// <summary>
	/// 移动目标细胞
	/// </summary>
	CellManager moveTargetCell;

	//细胞营背景图片
	SpriteRenderer cellSprite;
	//选中状态
	SpriteRenderer choosedSprite;

	/// <summary>
	/// 起点坐标
	/// </summary>
	Vector2[] splineStartPoints;
	/// <summary>
	/// 终点坐标
	/// </summary>
	Vector2[] splineEndPoints;
	/// <summary>
	/// 生成的间隔时间
	/// </summary>
	float delayTime = 0;

	const float EXCEEDWAITTIME = 1f;
	float exceedTime = 0f;
	//临时变量用来计算半径变化
	float tempRadius = 0; 

	#if CELLTEST
	UnityEngine.UI.Text testText;
	#endif

	public CellManager() {}

	public CellManager(CellAttribute cellAttribute, Transform root, List<CircleStrust> mcircleList, int id = -1) {
		mCellAttribute = cellAttribute;
		circleList = mcircleList;
		cellObj = PoolManager.Pools[PrefabPath.BATTLEPOOL].Spawn(PrefabPath.CELL).gameObject;
		if(id != -1) {
			cellObj.GetComponentInChildren<Collider>().name = id.ToString();
		}
		cellObj.transform.parent = root;
		cellObj.transform.localPosition = mCellAttribute.position;
		if(!cellObj.activeSelf) {
			cellObj.SetActive(true);
		}
		text = cellObj.GetComponentInChildren<UnityEngine.UI.Text>();
		if(cellObj.transform.childCount > 0) {
			cellBackground = cellObj.transform.GetChild(CELLBGINDEX);	
			cellSprite = cellBackground.transform.GetChild(0).GetComponent<SpriteRenderer>();
			choosedSprite = cellBackground.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>();
			cellBackground.localScale = new Vector3(mCellAttribute.radius * 2, mCellAttribute.radius * 2, 1);
		} else {
			SZDebug.Log("there is no CellBackground");
		}

		SetText(mCellAttribute.virusNum);
		SetChoosedState(false);
		SetColor(mCellAttribute.virsAttribute.color);

		#if CELLTEST
		testText = cellObj.transform.GetChild(2).GetChild(0).GetComponent<UnityEngine.UI.Text>();
		testText.gameObject.SetActive(true);
		#endif
	}


	public void SetText(float cellCounts) {
		if(text != null) {
			text.text = Mathf.CeilToInt(cellCounts).ToString();
		} else {
			Debug.LogError("there is no TextComponent");
		}
	}

	public void SetColor(int color) {
		if(cellSprite != null) {
			cellSprite.color = new Color(ConfigManager.getInstance.getVirusColorDic[color] .red / 255f, ConfigManager.getInstance.getVirusColorDic[color].green /  255f, ConfigManager.getInstance.getVirusColorDic[color] .blue / 255f, 1);
		}
	}

	public void SetChoosedState(bool choosedState) {
		if(choosedSprite != null) {
			choosedSprite.enabled = choosedState;
		}
	}

	public void IncreaseScale() {
		if(cellBackground != null) {
			tempRadius = mCellAttribute.radius  + Mathf.Sqrt((mCellAttribute.virusNum - mCellAttribute.content) / Mathf.PI * 0.1f ) ;
			tempRadius  = Mathf.MoveTowards(radius, tempRadius, Time.deltaTime);
			cellBackground.localScale =  new Vector3(tempRadius * 2, tempRadius * 2, 1 ) ;
		}
	}

	public void ReduceRadius() {
		if(cellBackground != null && radius > mCellAttribute.radius) {
			tempRadius = Mathf.MoveTowards(radius, mCellAttribute.radius, Time.deltaTime);
			cellBackground.localScale = new Vector3(tempRadius * 2, tempRadius * 2, 1);
		}
	}

	/// <summary>
	/// 细胞管理器更新
	/// </summary>
	public void Update() {
		if(mCellAttribute.virsAttribute.force != BattleConstDefine.NEUTRAL) {
			if(mCellAttribute.virusNum > mCellAttribute.content) {
				IncreaseScale();	
				exceedTime += Time.deltaTime;
				if(exceedTime > EXCEEDWAITTIME) {
					mCellAttribute.virusNum -=  (mCellAttribute.content * 0.1f + (mCellAttribute.virusNum - mCellAttribute.content) * 0.2f) * Time.deltaTime;
				}
			} else {
				exceedTime = 0;
				mCellAttribute.virusNum += (0.5f + 0.25f * mCellAttribute.repPlus * mCellAttribute.repPlus) * Time.deltaTime;
				ReduceRadius();
			}

		}
		SetText(mCellAttribute.virusNum);	
		#if CELLTEST
		testText.text = "cellID:" + mCellAttribute.ID + "content:" + mCellAttribute.content  + "\n" + "virsID:" + mCellAttribute.virsAttribute.ID;
		#endif
	}

	public void AddVirus(VirusAttribute virusM, float atk ) {
		if(mCellAttribute.virsAttribute.force == virusM.force) {
			mCellAttribute.virusNum += 1;
		} else {
			float defValue = BattleConstDefine.DEFAULTDEF + mCellAttribute.virsAttribute.def * mCellAttribute.defPlus;
			mCellAttribute.virusNum = (mCellAttribute.virusNum * defValue - atk) / defValue;
		}

		if(mCellAttribute.virusNum < 0) {
			mCellAttribute.virsAttribute =  virusM;
			mCellAttribute.virusNum = Mathf.Abs(mCellAttribute.virusNum);
			SetColor(mCellAttribute.virsAttribute.color);
			//被占领时停止生成细胞
			SZCoroutine.StopCoroutine(MoveViruses());
		}
	}


	/// <summary>
	/// 细胞释放病毒并移动
	/// </summary>
	/// <param name="targetCell">Target cell.</param>
	public void MoveViruses(CellManager targetCell) {
		this.moveTargetCell = targetCell;
		int virusesCounts = Mathf.FloorToInt(mCellAttribute.virusNum * 0.5f);
		float genarateTime =  GENERATETIME + (Mathf.Log(virusesCounts,2) - 1) *  TIMEINCREMENT;
		delayTime = genarateTime / virusesCounts;
		mCellAttribute.virusNum -= virusesCounts;
		splineStartPoints = GeometryTool.GenerateRandomPoint(cellPosition, mCellAttribute.radius, virusesCounts);
		splineEndPoints = GeometryTool.GenerateRandomFaceStart(targetCell.cellPosition, targetCell.mCellAttribute.radius, cellPosition, virusesCounts);
		SZCoroutine.StartCoroutine(MoveViruses());
	}
	/// <summary>
	/// 移动的协同程序
	/// </summary>
	IEnumerator MoveViruses() {
		for(int i = 0;i < splineStartPoints.Length; i ++) {
			LTSpline ltSpline = new SZSpline(circleList, splineStartPoints[i], splineEndPoints[i]).getSpline;
			VirusManager virus = new VirusManager(mCellAttribute.virsAttribute,this,moveTargetCell);
			virus.CellMove(ltSpline);
			yield return new WaitForSeconds(delayTime);
		}

	}

}
