﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 病毒管理器
/// </summary>
public class VirusManager  {


	/// <summary>
	/// 屏幕外的点
	/// </summary>
	float OUTSCREENPOS = -9999f;

	const float VIRUSZOOMTIME = 0.7f;
	const float VIRUSZOOMSCALEX = 0.35f;
	const float VIRUSZOOMSCALEY = 0.07f;
	//病毒背景
	GameObject virusBackground;
	//病毒体
	GameObject virusGameObject;
	//病毒核
	SpriteRenderer virusCore;
	//病毒壁
	SpriteRenderer virusBackSprite;
	//病毒属性
	public VirusAttribute virusAttribute;
	//病毒初始大小
	const float virusOriginRadius = 0.12f;
	LTSpline spline;
	float moveTotalTime = 0;
	float startMoveTime = 0;
	float endMoveTime = 0;
	float moveTime = 0;

	public CellManager mCellManager;
	public CellManager targetCellManager;

	public VirusManager(VirusAttribute mvirusAttribute,CellManager mCell, CellManager mTarget) {
		this.targetCellManager = mTarget;
		this.mCellManager = mCell;

		SpawnPool sp = PoolManager.Pools[PrefabPath.BATTLEPOOL];
		virusGameObject = sp.Spawn(PrefabPath.VIRUS).gameObject;
		virusGameObject.transform.position = new Vector3(OUTSCREENPOS, OUTSCREENPOS, OUTSCREENPOS);

		float angle = Mathf.Atan2((mTarget.cellPosition - mCell.cellPosition).x, (mTarget.cellPosition - mCell.cellPosition).y)  * Mathf.Rad2Deg;	
		virusGameObject.transform.localEulerAngles = new Vector3(0, 0,  90 - angle); 

		virusCore = virusGameObject.transform.GetChild(0).GetComponent<SpriteRenderer>();
		virusBackground = virusGameObject.transform.GetChild(1).gameObject;
		virusBackSprite = virusBackground.GetComponent<SpriteRenderer>();
		virusAttribute = mvirusAttribute;
		virusBackground.transform.localScale = new Vector3(virusOriginRadius, virusOriginRadius, 1);
		SetColor();
//		SZDebug.Log("init cell", SZDebug.Color.CYAN);
	}

	void SetColor() {
		Color color =  new Color(ConfigManager.getInstance.getVirusColorDic[virusAttribute.color ].red / 255f, ConfigManager.getInstance.getVirusColorDic[virusAttribute.color ].green /  255f, ConfigManager.getInstance.getVirusColorDic[virusAttribute.color ].blue / 255f, 1);
		virusCore.color = color;
		virusBackSprite.color =  new Color(color.r, color.g, color.b, color.a * 0.35f);
	}

	public void CellMove(LTSpline moveSpline) {
		
		this.spline = moveSpline;
		moveTotalTime = moveSpline.distance / (virusAttribute.spd + BattleConstDefine.DEFAULTSPEED);
		startMoveTime = 0;
		if(moveTotalTime - startMoveTime > VIRUSZOOMTIME) {
			endMoveTime = startMoveTime + VIRUSZOOMTIME;
			moveTime = VIRUSZOOMTIME;
		} else {
			endMoveTime = moveTotalTime;
			moveTime = endMoveTime - startMoveTime;
		}
		Move();	
	}

	void Move() {
		LeanTween.scale(virusBackground, new Vector3(VIRUSZOOMSCALEX, VIRUSZOOMSCALEY, 1), VIRUSZOOMTIME * 0.5f);
		LeanTween.scale(virusBackground, new Vector3(virusOriginRadius, virusOriginRadius, 1), VIRUSZOOMTIME * 0.5f).setDelay( VIRUSZOOMTIME * 0.5f);
		LeanTween.value(virusGameObject, startMoveTime / moveTotalTime, endMoveTime / moveTotalTime, moveTime).setOnUpdate(delegate(float obj) {
			float angle = Mathf.Atan2((spline.point(obj) - virusGameObject.transform.position).x, (spline.point(obj) - virusGameObject.transform.position).y)  * Mathf.Rad2Deg  ;
			float curangle = virusGameObject.transform.localEulerAngles.z;
			virusGameObject.transform.localEulerAngles = new Vector3(0, 0, Mathf.MoveTowardsAngle(curangle, 90 - angle, 90 * Time.deltaTime) ); 
			virusGameObject.transform.position = spline.point(obj);
			
		}).setOnComplete(delegate() {
			if(endMoveTime < moveTotalTime) {
				startMoveTime = endMoveTime;
				if(moveTotalTime - startMoveTime > VIRUSZOOMTIME) {
					endMoveTime = startMoveTime + VIRUSZOOMTIME;
					moveTime = VIRUSZOOMTIME;
				} else {
					endMoveTime = moveTotalTime;
					moveTime = endMoveTime - startMoveTime;
				}
				Move();
			} else {
//				SZDebug.Log("Move Completed!", SZDebug.Color.YELLOW);
				Destroy();
			}
		}).setEaseOutCubic();
	}

	void Destroy() {
		PoolManager.Pools[PrefabPath.BATTLEPOOL].Despawn(virusGameObject.transform);
		if(targetCellManager != null) {
			float atkvalue = BattleConstDefine.DEFAULTATK + virusAttribute.atk * mCellManager.mCellAttribute.atkPlus;
			targetCellManager.AddVirus(virusAttribute, atkvalue);
		}
	}
}
