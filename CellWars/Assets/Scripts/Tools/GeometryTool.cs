﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GeometryTool {
	public static bool JudgePointOnVectorsSide(Vector2 point, Vector2 vectorsStart, Vector2 vectorsEnd) {

		float temp = (vectorsEnd.x - vectorsStart.x) * (point.y - vectorsStart.y) - (point.x - vectorsStart.x) * (vectorsEnd.y - vectorsStart.y);
		if(temp > 0) {
			return true;	
		} else {
			return false;
		}
	}
	//转换3维坐标为2维坐标，忽略z轴	
	public static Vector2 ConvertVec3ToVec2(Vector3 original) {
		return new Vector2(original.x, original.y);
	}
	//转换2维坐标维为3维坐标，参数为z
	public static Vector3 ConverVec2ToVec3(Vector2 original, float axisZ = 0) {
		return new Vector3(original.x, original.y, axisZ);
	}

	//判断一个点是否在一个圆内
	public static bool CheckInCircle(Vector3 point, Vector3 circleCentre, float radius) {
		if(Vector3.Distance(point, circleCentre) < radius) {
			return true;
		} else {
			return false;
		}
	}
	//在圆内生成随机点
	const float INSIDEVALUE = 0.8f;
	public static Vector2[] GenerateRandomPoint(Vector2 centre, float radius, int counts) {
		Vector2[] randomPoints = new Vector2[counts];
		for(int i = 0; i < counts; i ++) {
			randomPoints[i] = centre + Random.insideUnitCircle * radius * INSIDEVALUE;
		}
		return randomPoints;
	}
	/// <summary>
	/// 圆内生成随机的一个点
	/// </summary>
	public static Vector2 GenerateRandomPoint(Vector2 centre, float radius) {
			return centre + Random.insideUnitCircle * radius * INSIDEVALUE;
	}


	const float INSIDESCALE = 0.8f;
	const float RADIANCONST = 0.5f;//1.57f;
	/// <summary>
	///在圆内生成面向起点的随机点
	/// </summary>
	public static Vector2[] GenerateRandomFaceStart(Vector2 centre, float radius, Vector2 startPoint, int counts) {
		Vector2[] randomPoints = new Vector2[counts];
		Vector2 normal = (startPoint - centre).normalized;
		float newX; 
		float newY;
		float randomRadian;
		for(int i = 0; i < counts; i ++) {
			randomRadian = Random.Range(-RADIANCONST, RADIANCONST);
			newX = (startPoint - centre).x * Mathf.Cos(randomRadian ) - (startPoint - centre).y * Mathf.Sin(randomRadian );
			newY = (startPoint - centre).y * Mathf.Cos(randomRadian ) + (startPoint - centre).x * Mathf.Sin(randomRadian );

			randomPoints[i] = centre + new Vector2(newX, newY).normalized * radius * INSIDESCALE;
		}
		return randomPoints;
	}

	//判断线段与圆相交，并且返回相交点
	public const float DEFAUTVALUE = 65536.0f;  
	public static bool LineInterCircle(
		Vector2 ptStart, // 线段起点  
		Vector2 ptEnd, // 线段终点  
		Vector2 ptCenter, // 圆心坐标  
		float radius,  //半径
		ref	Vector2 closePoint) {  

		if(CheckInCircle(ptStart, ptCenter, radius) || (CheckInCircle(ptEnd, ptCenter, radius))) {
			return false;
		}

		Vector2 ptInter2;
		closePoint.x = ptInter2.x = DEFAUTVALUE;
		closePoint.y = ptInter2.y = DEFAUTVALUE;  

		float fDis = Mathf.Sqrt((ptEnd.x - ptStart.x) * (ptEnd.x - ptStart.x) + (ptEnd.y - ptStart.y) * (ptEnd.y - ptStart.y));  

		Vector2 d;  
		d.x = (ptEnd.x - ptStart.x) / fDis;  
		d.y = (ptEnd.y - ptStart.y) / fDis;  

		Vector2 E;  
		E.x = ptCenter.x - ptStart.x;  
		E.y = ptCenter.y - ptStart.y;  

		float a = E.x * d.x + E.y * d.y;  
		float a2 = a * a;  

		float e2 = E.x * E.x + E.y * E.y;  

		float r2 = radius * radius;  

		if ((r2 - e2 + a2) < 0) {  
			return false;  
		} else {  
			float f = Mathf.Sqrt(r2 - e2 + a2);  

			float t = a - f;  

			if (((t - 0.0) > -0.00001f) && (t - fDis) < 0.00001f) {  
				closePoint.x = ptStart.x + t * d.x;  
				closePoint.y = ptStart.y + t * d.y;  
			}         

			t = a + f;  

			if (((t - 0.0) > -0.00001f) && (t - fDis) < 0.00001f) {  
				ptInter2.x = ptStart.x + t * d.x;  
				ptInter2.y = ptStart.y + t * d.y;  
			}  
			if(Vector2.SqrMagnitude(closePoint - ptStart) > Vector2.SqrMagnitude(ptInter2 - ptStart)) {
				closePoint = ptInter2;
			}

			closePoint -= (ptEnd - ptStart).normalized * 0.75f;
			if(closePoint.x == DEFAUTVALUE) {
				return false;
			}
			return true;  
		}  
	}


	//生成与传入向量垂直并且在圆上切距离向量近的点
	public const float OUTCIRCLEVALUE = 1.1f;
	public static Vector3 GenerateOutCirlePoint(Vector3 startPoint, Vector3 endPoint, Vector3 centre, float radius) {
		Vector3 normalizedVec =  Vector3.Cross(endPoint - startPoint, Vector3.forward).normalized;
		Vector3 outPoint1 = centre - normalizedVec * radius * OUTCIRCLEVALUE;
		Vector3 outPoint2 = centre + normalizedVec * radius * OUTCIRCLEVALUE;

		if(Vector3.Angle(outPoint1 - startPoint, endPoint - startPoint) <  Vector3.Angle(outPoint2 - startPoint, endPoint - startPoint)) {
			return outPoint1;
		} else {
			return outPoint2;
		}
	}
}
