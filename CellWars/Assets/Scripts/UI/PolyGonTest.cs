﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PolyGonTest :  BaseMeshEffect
{

	public Transform[] pos;
	public Color[] posColor;

	public override void ModifyMesh(VertexHelper vh)
	{
		
		vh.Clear();
		vh.AddVert(pos[0].localPosition, posColor[0], Vector2.zero);
		vh.AddVert(pos[1].localPosition, posColor[1], Vector2.zero);
		vh.AddVert(pos[2].localPosition, posColor[2], Vector2.zero);
		vh.AddVert(pos[3].localPosition, posColor[3], Vector2.zero);
		vh.AddVert(pos[4].localPosition, posColor[4], Vector2.zero);
		vh.AddVert(pos[5].localPosition, posColor[5], Vector2.zero);
		
		vh.AddTriangle(0, 1, 2);
		vh.AddTriangle(0, 2, 3);
		vh.AddTriangle(0, 3, 4);
		vh.AddTriangle(0, 4, 5);
		vh.AddTriangle(0, 5, 1);
	}

	void Update()
	{
		graphic.SetAllDirty();
	}
}
