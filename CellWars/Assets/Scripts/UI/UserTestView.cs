﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserTestView : ViewBase {
	[SerializeField]
	Button confirmBtn;
	[SerializeField]
	Button changeColor;
	[SerializeField]
	Slider atkSlider;
	[SerializeField]
	Slider defSlider;
	[SerializeField]
	Slider spdSlider;
	[SerializeField]
	Slider agiSlider;
	[SerializeField]
	Slider repSlider;
	[SerializeField]
	InputField chapterId;
	[SerializeField]
	PolyGonTest polyGonTest;
	[SerializeField]
	Text[] textField;

	public override void Create(BaseContext context)
	{
		atkSlider.value = DataManager.getInstance.currentVirus.atk;
		defSlider.value = DataManager.getInstance.currentVirus.def;
		agiSlider.value = DataManager.getInstance.currentVirus.agi;
		repSlider.value = DataManager.getInstance.currentVirus.rep;
		spdSlider.value = DataManager.getInstance.currentVirus.spd;

		textField[0].text = "ATK:" + Mathf.FloorToInt(DataManager.getInstance.currentVirus.atk);
		textField[1].text = "DEF:" + Mathf.FloorToInt(DataManager.getInstance.currentVirus.def);
		textField[2].text = "AGI:" + Mathf.FloorToInt(DataManager.getInstance.currentVirus.agi);
		textField[3].text = "REP:" + Mathf.FloorToInt(DataManager.getInstance.currentVirus.rep);
		textField[4].text = "SPD:" + Mathf.FloorToInt(DataManager.getInstance.currentVirus.spd);

		polyGonTest.pos[1].position = atkSlider.targetGraphic.transform.position;
		polyGonTest.pos[2].position = defSlider.targetGraphic.transform.position;
		polyGonTest.pos[3].position = agiSlider.targetGraphic.transform.position;
		polyGonTest.pos[4].position = repSlider.targetGraphic.transform.position;
		polyGonTest.pos[5].position = spdSlider.targetGraphic.transform.position;

		SetColor(DataManager.getInstance.currentVirus.color);

		OnRegister();
	}
	public override void Close(BaseContext context = null)
	{
		
	}
	public override void OnRegister()
	{
		atkSlider.onValueChanged.AddListener(AtkValueChange);
		defSlider.onValueChanged.AddListener(DefValueChange);
		spdSlider.onValueChanged.AddListener(SpdValueChange);
		agiSlider.onValueChanged.AddListener(AgiValueChange);
		repSlider.onValueChanged.AddListener(RepValueChange);
		confirmBtn.onClick.AddListener(ConfirmBtn);
		changeColor.onClick.AddListener (RandomColor);
		chapterId.onValueChanged.AddListener(SetChapterId);
	}
	public override void OnUnRegister()
	{
		base.OnUnRegister();
	}

	void AtkValueChange(float atk) {
		polyGonTest.pos[1].position = atkSlider.targetGraphic.transform.position;
		int curAtk = Mathf.FloorToInt(atk);
		textField[0].text = "ATK:" + curAtk;
		DataManager.getInstance.currentVirus.atk = curAtk;
	}
	void DefValueChange(float def) {
		polyGonTest.pos[2].position = defSlider.targetGraphic.transform.position;
		int curDef = Mathf.FloorToInt(def);
		textField[1].text = "DEF:" + curDef;
		DataManager.getInstance.currentVirus.def = curDef;
	}

	void AgiValueChange(float agi){
		polyGonTest.pos[3].position = agiSlider.targetGraphic.transform.position;
		int curAgi = Mathf.FloorToInt(agi);
		textField[2].text = "AGI:" + curAgi;
		DataManager.getInstance.currentVirus.agi = curAgi;
	}
	void RepValueChange(float rep) {
		polyGonTest.pos[4].position = repSlider.targetGraphic.transform.position;
		int curRep = Mathf.FloorToInt(rep);
		textField[3].text = "REP:" + curRep;
		DataManager.getInstance.currentVirus.rep = curRep;
	}
	void SpdValueChange(float spd){
		polyGonTest.pos[5].position = spdSlider.targetGraphic.transform.position;
		int curSpd = Mathf.FloorToInt(spd);
		textField[4].text = "SPD:" + curSpd;
		DataManager.getInstance.currentVirus.spd = curSpd;
	}

	void SetColor(int colorIndex) {
		VirsColor virColor = ConfigManager.getInstance.getVirusColorDic[colorIndex];
		Color color = new Color(virColor.red / 255f, virColor.green / 255f, virColor.blue / 255f, 1);
		for(int i = 0; i < polyGonTest.posColor.Length; i ++) {
			polyGonTest.posColor[i] = new Color(color.r, color.g, color.b, polyGonTest.posColor[i].a);
		}
	}

	void SetChapterId(string chapterId) {
		SZDebug.Log(chapterId);
		int innerChapterId = 0;
		if(int.TryParse(chapterId, out innerChapterId)) {
			DataManager.getInstance.currentChapterId = innerChapterId;	
		}
	}

	void ConfirmBtn() {
		if(ConfigManager.getInstance.getChapterConfigDic.ContainsKey(DataManager.getInstance.currentChapterId)) {
			GameController.getInstance.ChangeState(BattleState.getInstance);
			UIManager.getInstance.CloseView(UIType.UserTestView);	
		} else {
			chapterId.text = "没有这个关卡ID！！！";
		}

	}

	void RandomColor() {
		int colorCounts = ConfigManager.getInstance.getVirusColorDic.Count;
		if(DataManager.getInstance.currentVirus.color < colorCounts) {
			DataManager.getInstance.currentVirus.color ++;
		} else {
			DataManager.getInstance.currentVirus.color = 1;
		}
		SetColor(DataManager.getInstance.currentVirus.color);
	}

}

public class UserTestContext : BaseContext {
	public UserTestContext() : base(UIType.UserTestView) {
		
	}
}