﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathManager
{
	const int ASSEETNAMELENGTH = 7;
	const string CONFIGPATHNAME = "/Documents/Config/";
	private static string configPath;

	public static string getConfigPath {
		get {
			#if UNITY_EDITOR
			configPath = Application.dataPath.Substring(0, Application.dataPath.Length);
			configPath += CONFIGPATHNAME;
			#elif UNITY_IOS
			configPath = Application.persistentDataPath + "/";
			#elif UNITY_ANDROID
			configPath = Application.persistentDataPath + "/";
			#else
			configPath = Application.dataPath.Substring (0, Application.dataPath.Length - ASSEETNAMELENGTH);
			configPath += CONFIGPATHNAME;
			#endif
			Tools.CreateDirectory(configPath);
			return configPath;
		}
	}


}
