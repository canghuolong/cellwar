﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleConstDefine  {
	/// <summary>
	/// 中立
	/// </summary>
	public const int NEUTRAL = 0;
	/// <summary>
	/// 己方
	/// </summary>
	public const int SELFFORCE = 1;
	/// <summary>
	/// 敌方
	/// </summary>
	public const int ENEMY = 2;

	/// <summary>
	/// 默认攻击
	/// </summary>
	public const float DEFAULTATK = 2f;
	/// <summary>
	/// 默认防御
	/// </summary>
	public const float DEFAULTDEF = 2f;
	/// <summary>
	/// 病毒默认移动速度
	/// </summary>
	public const float DEFAULTSPEED = 0f;
}
