﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SZSpline  {
	const float MINVALUE = -9999f;
	List<CircleStrust> circleList;
	List<Vector2> splineList = new List<Vector2>();
	LTSpline spline = null;
	public LTSpline getSpline {
		get {
			return spline;
		}
	}

	public SZSpline(List<CircleStrust> mList, Vector2 mStartP, Vector2 mEndP) {
		circleList = mList;
		splineList.Add(mStartP);
		splineList.Add(mStartP);
		GetSpline(mStartP, mEndP);
	}

	void GetSpline(Vector2 mStartP, Vector2 mEndP) {
		Vector2 intersect = Vector2.zero;
		Vector2 finalPoint = new Vector2(MINVALUE, MINVALUE);
		Vector2 pointOnCircle = new Vector2(MINVALUE, MINVALUE);
		int tempId = -1;
		for(int i = 0; i < circleList.Count; i ++) {
			if (GeometryTool.LineInterCircle(mStartP, mEndP, circleList[i].centre, circleList[i].radius, ref intersect)) {
				if (Vector3.Distance(mStartP, intersect) < Vector3.Distance(mStartP, finalPoint)) {
					finalPoint = intersect;
					tempId = i;
				}
			}
		}
		if(finalPoint.x == MINVALUE) {
//			Debug.Log("spline count:" + splineList.Count + "VecStart:" + mStartP + "VecEnd:" + mEndP );	
			splineList.Add(mEndP);
			splineList.Add(mEndP);
			int length = splineList.Count;
			Vector3[] splineArray = new Vector3[length];
			if(length >= 4) {
				for(int j = 0; j < splineList.Count; j ++) {
					splineArray[j] = new Vector3(splineList[j].x, splineList[j].y, 0); 
				}
				splineArray[0] = splineArray[0] + (splineArray[1] - splineArray[2]).normalized;
				splineArray[length - 1] = splineArray[length - 1] +  (splineArray[length - 2] - splineArray[length - 3]).normalized;
			} else {
				splineArray = new Vector3[4];
				splineArray[0] = new Vector3(mStartP.x, mStartP.y, 0);
				splineArray[1] = new Vector3(mStartP.x, mStartP.y, 0);
				splineArray[2] = new Vector3(mEndP.x, mEndP.y, 0);
				splineArray[3] = new Vector3(mEndP.x, mEndP.y, 0);
			}
			spline = new LTSpline(splineArray);

		} else {
			

			pointOnCircle = GeometryTool.GenerateOutCirlePoint(GeometryTool.ConverVec2ToVec3(mStartP),GeometryTool.ConverVec2ToVec3(mEndP), circleList[tempId].centre, circleList[tempId].radius);
			if(Vector2.Angle((finalPoint - mStartP), (pointOnCircle - mStartP)) > 3) {
				splineList.Add(finalPoint);	
			}
			splineList.Add(pointOnCircle);
			GetSpline(pointOnCircle, mEndP);
		}
	}
}
