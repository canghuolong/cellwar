﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleManager {
	/// <summary>
	/// 细胞默认Z轴位置
	/// </summary>
	const float CELLPOSZ = 15;
	/// <summary>
	/// 战场
	/// </summary>
	public Transform battleField;
	///<summary>
	/// 战场相机
	/// </summary>
	private Camera battleCamera;
	/// <summary>
	/// 细胞数组
	/// </summary>
	public List<CellManager> cellManagerList;
	/// <summary>
	/// 圆圈数组
	/// </summary>
	public List<CircleStrust> circleList;

	/// <summary>
	/// 细胞行动轨迹线数组
	/// </summary>
	public List<Transform> lineArray;
	/// <summary>
	/// 连线的起点数组
	/// </summary>
	Vector3[] startPoints;
	/// <summary>
	///连线的终点数组
	/// </summary>
	Vector3[] endPoints;
	/// <summary>
	/// 选中自己的细胞
	/// </summary>
	bool haveChooseCell = false;
	/// <summary>
	/// 选中的细胞
	/// </summary>
	List<CellManager> choosedCells = new List<CellManager>();
	/// <summary>
	/// 当前选中的细胞ID
	/// </summary>
	int currentCellID = -1;
	const int NOHITEDID = -1;
	/// <summary>
	/// 屏幕触碰点映射到世界的坐标
	/// </summary>
	Vector3 screenPointVec;
	/// <summary>
	/// 射线发射长度
	/// </summary>
	const float RAYLENGTH = 110f;
	/// <summary>
	/// 屏幕映射到世界的Z轴距离
	/// </summary>
	const float SCREENDISTANCE = 15;


	/// <summary>
	/// 更新代理
	/// </summary>
	public  System.Action updateDelegate;

	public BattleManager() {
		Init();
	}

	public void Destroy() {
		updateDelegate = null;
	} 

	public void Update() {
		UserInput();
		if(updateDelegate != null) {
			updateDelegate();
		}
	}


	void Init() {
		battleField = new GameObject("BattleField").transform;
		battleCamera = GameObject.FindGameObjectWithTag(TagConst.BATTLECAMERA).GetComponent<Camera>();
		lineArray = new List<Transform>();

		cellManagerList = new List<CellManager>();
		circleList = new List<CircleStrust>();

		ChapterData cd = DataManager.getInstance.GetChapterData(DataManager.getInstance.currentChapterId);

		for(int i = 0; i < cd.cellData.Count; i ++) {
			string[] cellDataStr = cd.cellData[i].Split('|');
			string[] posStr = cellDataStr[0].Split(',');
			Vector3 cellPos = new Vector3(float.Parse(posStr[0]), float.Parse(posStr[1]), CELLPOSZ);
			VirsAblity virsConfig = ConfigManager.getInstance.getVirusConfigDic[int.Parse(cellDataStr[1])];
			VirusAttribute virs;
			if(virsConfig.force == BattleConstDefine.SELFFORCE) {
				virs = DataManager.getInstance.currentVirus.SetOtherAttribute(virsConfig);
			} else {
				virs = new VirusAttribute(virsConfig);
			}

			CellsType cellsType = ConfigManager.getInstance.getCellConfigDic[int.Parse(cellDataStr[2])];
			CellAttribute cellAtb = new CellAttribute(cellsType, cellPos, virs);

			CellManager tempCellManager = new CellManager(cellAtb, battleField, circleList, i);
			updateDelegate += tempCellManager.Update;
			circleList.Add(new CircleStrust(tempCellManager.cellPosition, tempCellManager.radius));
			cellManagerList.Add(tempCellManager);
		}
	}


	void UserInput() {
		if (Input.GetButtonDown("Fire1")) {
			RaycastHit hit;
			Ray ray = battleCamera.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out hit, RAYLENGTH)) {
				SZDebug.Log("hit collider name is " + hit.collider.name + "  Force:" + cellManagerList[int.Parse(hit.collider.name)].getForce, SZDebug.Color.MAGENTA);
				if (CheckIsYourCell(hit.collider.name)) {
					currentCellID = int.Parse(hit.collider.name);
					haveChooseCell = true;
					choosedCells.Add(cellManagerList[currentCellID]);
				}
			}
		}



		if (Input.GetButton("Fire1") && haveChooseCell) {
			RaycastHit hit;
			Ray ray = battleCamera.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out hit, RAYLENGTH)) {
				currentCellID = int.Parse(hit.collider.name);
				if (CheckIsYourCell(hit.collider.name)) {
					if (!choosedCells.Contains(cellManagerList[currentCellID])) {
						choosedCells.Add(cellManagerList[currentCellID]);	
					}
				} else {
					cellManagerList[currentCellID].SetChoosedState(true);
				} 
			} else {
//						SZDebug.Log("current id: " + currentCellID);
				if (currentCellID != NOHITEDID) {
					cellManagerList[currentCellID].SetChoosedState(false);
				}
				currentCellID = NOHITEDID;
			}
			SetChooseCellState(true);
			screenPointVec = battleCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, SCREENDISTANCE));
			InitLines(choosedCells.Count);

			startPoints = new Vector3[choosedCells.Count];
			endPoints = new Vector3[choosedCells.Count];
			ResetStartsAndEnds(screenPointVec);
			DrawLine(startPoints, endPoints);
		}

		if (Input.GetButtonUp("Fire1") && haveChooseCell) {
			RaycastHit hit;
			Ray ray = battleCamera.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out hit, RAYLENGTH)) {
				currentCellID = int.Parse(hit.collider.name);
				if (CheckIsYourCell(hit.collider.name)) {
					if (choosedCells.Contains(cellManagerList[currentCellID])) {
						choosedCells.Remove(cellManagerList[currentCellID]);	
					}
				} 
				cellManagerList[currentCellID].SetChoosedState(false);
				for(int i = 0; i < choosedCells.Count; i ++) {
					choosedCells[i].MoveViruses(cellManagerList[currentCellID]);
				}

			} else {
				currentCellID = NOHITEDID;
			}
			haveChooseCell = false;
			SetChooseCellState(false);
			SetLinesEnable(false);
			choosedCells.Clear();
		}
	}

	/// <summary>
	/// 检测细胞的阵营
	/// </summary>
	bool CheckIsYourCell(string hitName) {
		int index;
		if(int.TryParse(hitName, out index)) {
			if(index >= cellManagerList.Count) {
				SZDebug.Log("hit collider is incorrect");	
			} else {
				if(cellManagerList[index].getForce == CellManager.Force.SELF) {
					return true;
				}
			}
		}
		return false;
	}
	/// <summary>
	/// 设置细胞的选中状态
	/// </summary>
	void SetChooseCellState(bool state) {
		for(int i = 0; i < choosedCells.Count; i ++) {
			choosedCells[i].SetChoosedState(state);
		}
	}
		
	/// <summary>
	/// 初始化轨迹线
	/// </summary>
	void InitLines(int lineCounts) {
		int lineArrayCount = lineArray.Count;
		for(int i = lineArrayCount; i < lineCounts; i ++) {
			Transform tempLine = PoolManager.Pools[PrefabPath.BATTLEPOOL].Spawn(PrefabPath.LINE);
			tempLine.SetParent(battleField);
			lineArray.Add(tempLine);
		}
	}
		
	/// <summary>
	/// 画运动轨迹线
	/// </summary>
	void DrawLine(Vector3[] startPoints, Vector3[] endPoints) {
		for(int i = 0; i < startPoints.Length; i ++) {
			float distance = Vector3.Distance(startPoints[i], endPoints[i]);
			if(distance == 0) {
				lineArray[i].gameObject.SetActive(false);
			} else {
				lineArray[i].localPosition = startPoints[i];
				lineArray[i].localScale = new Vector3(distance,1, 1);
				float angle = Mathf.Atan2( (endPoints[i] - startPoints[i]).x, (endPoints[i] - startPoints[i]).y) * Mathf.Rad2Deg;
				lineArray[i].transform.localEulerAngles = new Vector3(0, 0, 90 - angle);
				lineArray[i].gameObject.SetActive(true);
			}
		}
	}
	/// <summary>
	/// 设置轨迹线的状态
	/// </summary>
	void SetLinesEnable(bool enable) {
		for(int i = 0; i < lineArray.Count; i ++) {
			lineArray[i].gameObject.SetActive(enable);
		}
	}


	/// <summary>
	/// 重新设置连线的起点和终点
	/// </summary>
	void	ResetStartsAndEnds(Vector3 point) {
		CellManager tempCellManager = null;
		if(currentCellID != NOHITEDID) {
			tempCellManager = cellManagerList[currentCellID];
		}

		for(int i = 0; i < choosedCells.Count; i ++) {
			startPoints[i] = choosedCells[i].cellPosition;
		}

		if(tempCellManager != null) {
			for(int i = 0; i < startPoints.Length; i ++) {
				if(tempCellManager != choosedCells[i]) {
					startPoints[i] += (tempCellManager.cellPosition - choosedCells[i].cellPosition).normalized * choosedCells[i].radius;
					endPoints[i] = tempCellManager.cellPosition - (tempCellManager.cellPosition - startPoints[i]).normalized * tempCellManager.radius;	
				} else {
//					Debug.Log("point is in the circle!!!");
					startPoints[i] = endPoints[i] = Vector3.zero;
				}
			}
		} else {
			for(int i = 0; i < startPoints.Length; i ++) {
				startPoints[i] += (point - choosedCells[i].cellPosition).normalized * choosedCells[i].radius;
				endPoints[i] = point;
			}
		}
	}
}
