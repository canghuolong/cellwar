﻿//测试战斗专用脚本
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartBattleWithoutEnter : MonoBehaviour {
	void Awake() {
		if(GameController.CheckNormalStart()) {
			Destroy(gameObject);
		}
	}

	void Start() {
		GameController.getInstance.ChangeState(BattleState.getInstance);
	}
}
