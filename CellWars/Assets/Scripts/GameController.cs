﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {
	
	#region GameController	
	/// <summary>
	/// 游戏控制器
	/// </summary>
	private static GameController instance;
	public static GameController getInstance {
		get {
			if (instance == null) {
				instance = new GameObject ("GameController").AddComponent<GameController> ();
			}
			return instance;
		}
	}
	#endregion

	#region StateMachine
	StateMachine<GameController> mStateMachine;
	public State<GameController> getState {
		get {
			return mStateMachine.CurrentState;
		}
	}
	#endregion

	#region StateList
	State<GameController>[] enterGameStateList = new State<GameController>[]
	{
		InitState.getInstance
	};

	#endregion

	void Awake() {
		DontDestroyOnLoad(this);
		instance = this;
		mStateMachine = new StateMachine<GameController>(this);
		mStateMachine.SetInitState(enterGameStateList[0]);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		mStateMachine.StateMachineUpdate();
	}

	public void ChangeState(State<GameController> state) {
		mStateMachine.ChangeState(state);
	}

	public static bool CheckNormalStart() {
		if(instance == null) {
			return false;
		} else 
			return true;
	}
}
