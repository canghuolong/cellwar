﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorConst : ScriptableObject {
	public Color whiteCell;
	public Color blueCell;
	public Color blackCell;
	public Color redCell;
	public Color yellowCell;
	public Color purpleCell;
	public Color greenCell;
	public Color indigoCell;
	public Color grayCell;
}
