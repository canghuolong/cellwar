﻿using System.Collections;
using System.Collections.Generic;

public class PrefabPath {
	/// <summary>
	/// 池管理器
	/// </summary>
	public const string POOLMANAGER = "Prefabs/PoolManager";
	/// <summary>
	/// 战斗池管理
	/// </summary>
	public const string BATTLEPOOL = "Battle";
	/// <summary>
	/// 病毒
	/// </summary>
	public const string VIRUS = "Virus";
	/// <summary>
	/// 细胞
	/// </summary>
	public const string CELL = "Cell";
	/// <summary>
	/// 细胞连线
	/// </summary>
	public const string LINE = "Line";
}
