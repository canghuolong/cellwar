﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainState : State<GameController> {
	static MainState instance;
	public static MainState getInstance {
		get {
			if(instance == null) {
				instance = new MainState();
			}
			return instance;
		}
	}
	public override void Enter(GameController type) {
		SZDebug.Log("enter main state", SZDebug.Color.BLUE);
		DataManager.getInstance.currentVirus = new VirusAttribute(1, 1, 1, 1, 1, 2);
		UIManager.getInstance.CreateView(UIType.UserTestView);
	}
	public override void Execute(GameController type) {
		
	}
	public override void Exit(GameController type) {
		
	}
}
