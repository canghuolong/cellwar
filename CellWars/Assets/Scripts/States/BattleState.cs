﻿using System.Collections;
using System.Collections.Generic;

public class BattleState : State<GameController>
{
	static BattleState instance;

	public static BattleState getInstance {
		get {
			if (instance == null) {
				instance = new BattleState();
			}
			return instance;
		}
	}
	BattleManager battleManager;
	public override void Enter(GameController type) {

		SZDebug.Log("init battle state", SZDebug.Color.BLUE);
		SZSceneManager.LoadLevelAsync(SceneNames.BATTLESCENE, delegate {
			SZDebug.Log("Real Enter BattleScene", SZDebug.Color.BLACK);
			battleManager = new BattleManager();
		});

	}

	public override void Execute(GameController type) {
		if(battleManager != null) {
			battleManager.Update();
		}
	}

	public override void Exit(GameController type) {
		battleManager.Destroy();
		battleManager = null;
	}
}
