﻿using System.Collections;
using System.Collections.Generic;

public class InitState : State<GameController> {
	static InitState instance;
	public static InitState getInstance {
		get {
			if(instance == null) {
				instance = new InitState();
			}
			return instance;
		}
	}

	public override void Enter(GameController type)
	{
		SZDebug.Log("enter init state", SZDebug.Color.BLUE);
		GameController.getInstance.ChangeState(MainState.getInstance);

	}


	public override void Execute(GameController type)
	{
		
	}

	public override void Exit(GameController type)
	{
		
	}
}

