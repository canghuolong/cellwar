﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

public static class JsonReader {
	public static MiniJsonData GetConfig(string configPath) {
		string dataString = null;
		string realConfigPath = configPath;
		if(File.Exists (realConfigPath)) {
			using(StreamReader streamReader = File.OpenText(realConfigPath)) {
				dataString = streamReader.ReadToEnd();
			}
		}
		if(dataString == null) {
			return null;
		}else {
			MiniJsonData tempData = MiniJsonData.Parse(dataString);
			return tempData;
		}
	}
}
