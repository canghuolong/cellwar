using UnityEngine;
using System.Collections;


public class SZSceneManager {
	private static string m_LoadedLevelName;
	public static string loadedLevelName {
		get {
			return m_LoadedLevelName;
		}
		
		private set {
			m_LoadedLevelName = value;
		}
	}

	public static void LoadLevel(string levelName) {
		loadedLevelName = levelName;
		UnityEngine.SceneManagement.SceneManager.LoadScene(levelName);
		SZDebug.Log("LoadLevel:" + levelName, SZDebug.Color.RED);
	}
	
	public static void LoadLevelAsync(string levelName, System.Action callBack = null) {
		SZDebug.Log("LoadLevel Async:" + levelName, SZDebug.Color.RED);
		loadedLevelName = levelName;
		SZCoroutine.StartCoroutine(LoadLevelAsyncCallBack(callBack));
	}

	static IEnumerator LoadLevelAsyncCallBack(System.Action callBack) {
		AsyncOperation ao = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(loadedLevelName);
		yield return ao;
		if(callBack != null) {
			callBack();	
		}
	}
}
