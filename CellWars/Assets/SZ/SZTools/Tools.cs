﻿//----------------------------------------------
//           小工具(完全基于Unity Api，不涉及业务逻辑)
//           AUTHOR: Sun Shubin
//----------------------------------------------
using System.Collections;
using System.Collections.Generic;

public class Tools  {
	#region 创建文件夹
	public static void CreateDirectory (string _path)
	{
		if (!System.IO.Directory.Exists (_path)) {
			System.IO.Directory.CreateDirectory (_path);	
		}
	}
	#endregion
}
