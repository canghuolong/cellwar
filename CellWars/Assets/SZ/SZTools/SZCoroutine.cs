﻿//----------------------------------------------
//         	协同函数封装类 
// 			Author 			:	SunShubin
//          用于统一管理协同函数，以及在非继承MonoBehaviour的类里面调用协同函数方法
//----------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class SZCoroutineHelper : MonoBehaviour{
	private static SZCoroutineHelper instance;

	public static SZCoroutineHelper getInstance {
		get {
			if (instance == null) {
				GameObject go = new GameObject("CoroutineHelper");
				instance = go.AddComponent<SZCoroutineHelper>();
			}
			return instance;
		}
	}

	void Awake(){
		DontDestroyOnLoad(this);	
	}

}

static internal class SZCoroutine{
	public static void StartCoroutine(string methodName){
		SZCoroutineHelper.getInstance.StartCoroutine(methodName);
	}

	public static void StartCoroutine(IEnumerator routine){
		SZCoroutineHelper.getInstance.StartCoroutine(routine);
	}

	public static void StartCoroutine(string methodName, object value){
		SZCoroutineHelper.getInstance.StartCoroutine(methodName, value);
	}

	public static void StopCoroutine(string methodName){
		SZCoroutineHelper.getInstance.StopCoroutine(methodName);
	}

	public static void StopCoroutine(IEnumerator routine){
		SZCoroutineHelper.getInstance.StopCoroutine(routine);
	}

	public static void StopCoroutine(Coroutine routine){
		SZCoroutineHelper.getInstance.StopCoroutine(routine);
	}

	public static void StopAllCoroutines(){
		SZCoroutineHelper.getInstance.StopAllCoroutines();
	}
}