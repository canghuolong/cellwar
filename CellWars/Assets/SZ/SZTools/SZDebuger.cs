﻿//----------------------------------------------
//         	包装Debug
// 			Author 			:	SunShubin
//----------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SZDebug {
	//颜色枚举
	public enum Color{
		WHITE,
		BLACK,
		BLUE,
		CYAN,
		GRAY,
		GREEN,
		YELLOW,
		RED,
		MAGENTA,
		GREY
	}

	#if CLOSELOG
	public static void Log(object message) {}

	public static void Log(object message, Color color) {}

	public static void Log(object message, Object context) {}

	public static void Log(object message, Object context, SZDebug.Color color) {}
	#else
	public static void Log(object message) {
		Debug.Log(message);
	}

	public static void Log(object message, SZDebug.Color color) {
		Debug.Log(TranslateColorToString(color).Replace("{SZDEBUGER}", message.ToString()));
	}

	public static void Log(object message, Object context) {
		Debug.Log(message, context);
	}

	public static void Log(object message, Object context, SZDebug.Color color) {
		Debug.Log(TranslateColorToString(color).Replace("{SZDEBUGER}", message.ToString()), context);
	}
	#endif


	public static void LogWarning(object message) {
		Debug.LogWarning(message);
	}

	public static void LogWarning(object message, Object context) {
		Debug.LogWarning(message, context);
	}

	public static void LogError(object message) {
		Debug.LogError(message);
	}

	public static void LogError(object message, Object context) {
		Debug.LogError(message, context);
	}

	static string TranslateColorToString(SZDebug.Color color) {
		switch(color) {
			case SZDebug.Color.WHITE :
				return "<color=white>{SZDEBUGER}</color>";
			case SZDebug.Color.BLACK :
				return "<color=black>{SZDEBUGER}</color>";
			case SZDebug.Color.BLUE:
				return "<color=blue>{SZDEBUGER}</color>";
			case SZDebug.Color.CYAN:
				return "<color=cyan>{SZDEBUGER}</color>";
			case SZDebug.Color.GRAY:
				return "<color=gray>{SZDEBUGER}</color>";
			case SZDebug.Color.GREEN:
				return "<color=green>{SZDEBUGER}</color>";
			case SZDebug.Color.YELLOW:
				return "<color=yellow>{SZDEBUGER}</color>";
			case SZDebug.Color.RED :
				return "<color=red>{SZDEBUGER}</color>";
			case SZDebug.Color.MAGENTA:
				return "<color=magenta>{SZDEBUGER}</color>";
			case SZDebug.Color.GREY:
				return "<color=grey>{SZDEBUGER}</color>";
			default :
				return "<color=white>{SZDEBUGER}</color>";
		}
	}
}
