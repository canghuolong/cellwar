using UnityEngine;
using System.Collections;

//base state
public abstract class State<T>
{
	//T target
	public T Target;
	//Enter 第一次设置时会调用
	public abstract void Enter(T type);
	//Execute 每次update时调用
	public abstract void Execute(T type);
	//Exit 退出时调用
	public abstract void Exit(T type);
}


