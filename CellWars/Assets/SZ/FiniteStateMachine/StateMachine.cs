//----------------------------------------------
//         	状态机
// 			Author 			:	SunShubin
//----------------------------------------------
using UnityEngine;
using System.Collections;

public class StateMachine<T>
{
		//StateMachine Owner
		private T m_Owner;

		private State<T> m_CurrentState;
		private State<T> m_PreviousState;

		/// <summary>
		/// 当前状态
		/// </summary>
		public State<T> CurrentState {
				get {
						return m_CurrentState;
				} 
        
		}
		
		/// <summary>
		/// 上一个状态
		/// </summary>
		public State<T> PreviousState {
				get {
						return m_PreviousState;
				}
       
		}
		/// <summary>
		/// 初始化
		/// </summary>
		public StateMachine (T owner)
		{
				m_Owner = owner;
				m_CurrentState = null;
				m_PreviousState = null;
		}

		public void SetInitState (State<T> CurrentState)
		{
				m_CurrentState = CurrentState;
				m_CurrentState.Target = m_Owner;
				m_CurrentState.Enter (m_Owner);
		}

		public void StateMachineUpdate ()
		{
				if (m_CurrentState != null)
						m_CurrentState.Execute (m_Owner);
		}

		public void ChangeState (State<T> newState)
		{
				if (newState == null) {
						Debug.LogError ("the state is not exist...");
				}

				m_CurrentState.Exit (m_Owner);
		
				m_PreviousState = m_CurrentState;
		
				m_CurrentState = newState;

				m_CurrentState.Target = m_Owner;
		
				m_CurrentState.Enter (m_Owner);
		}
		//Revert State
		public void RevertToPreviousState ()
		{
				ChangeState (m_PreviousState);
		
		}
}
