﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdapterManager : MonoBehaviour {
	//标准UI比例
	const float SCREENWIDTH = 1280f;
	const float SCREENHEIGHT = 720f;
	//适配模式
	public enum AdapterType {
		ADAPTIVEWIDTH = 0,
		ADAPTIVEHEIGHT = 1,
		ADAPTIVEALL = 2
	}
	public AdapterType mType = AdapterType.ADAPTIVEALL;
	float aspectRatio;
	float currentRatio;

	Vector3 mScale;
	Transform mTransform;
	void Awake () {
		mTransform = this.transform;
		mScale = mTransform.localScale;
		aspectRatio = (float)SCREENWIDTH / SCREENHEIGHT;
		currentRatio = (float)Screen.width / Screen.height;

		if(mType == AdapterType.ADAPTIVEALL) {
			if(currentRatio > aspectRatio) {
				float realRatio = currentRatio / aspectRatio;
				mScale = new Vector3(mScale.x * realRatio, mScale.y * realRatio, mScale.z);
			}
		} else if(mType == AdapterType.ADAPTIVEHEIGHT) {
			
		} else {
			
		}
		mTransform.localScale = mScale;
	}
}
