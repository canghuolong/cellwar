﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class DictionaryExtension{

	/// <summary>
	/// 将键和值添加到字典中：如果不存在，添加；存在，不添加
	/// </summary>
	public static Dictionary<TKey, TValue> TryAdd<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key, TValue value)
	{
		if (!dict.ContainsKey(key)) {
			dict.Add(key,value);
		} 
		return dict;
	}
	/// <summary>
	/// 将键和值添加或替换到字典中：如果不存在，添加；存在，则替换
	/// </summary>
	public static Dictionary<TKey, TValue> AddOrReplace<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key, TValue value)
	{
		dict[key] = value;
		return dict;
	}
}