﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameObjectExtandsion {
	/// <summary>
	///	设置一个物体和其子物体的层
	/// </summary>
	public static void ChangeLayer(this GameObject gameObj, int layer){
		gameObj.layer = layer;
		if (gameObj.transform.childCount > 0) {
			foreach (Transform trans in gameObj.transform) {
				trans.gameObject.ChangeLayer(layer);
			}	
		}
	}
}