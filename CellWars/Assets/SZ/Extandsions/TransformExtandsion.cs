﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TransformExtandsion {
	/// <summary>
	///查找物体深层的子物体
	/// </summary>
	public static Transform FindDeepChild(this Transform trans, string name) {
		Transform child = trans.Find(name);
		if(child == null) {
			foreach(Transform tempTran in trans) {
				child = tempTran.FindDeepChild(name);
				if(child != null) {
					return child;
				}
			}
		}
		return child;
	}
}