﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIType
{

	public string path { get; private set; }

	public string name { get; private set; }

	public Sysdefine.ViewType viewType{ get; private set; }

	public Sysdefine.ViewMode viewMode = Sysdefine.ViewMode.NORMAL;

	public bool IsClearReverseChange = false;

	
	public UIType(string uiPath, Sysdefine.ViewType uiType, Sysdefine.ViewMode uiMode) {
		path = uiPath;
		name = path.Substring(path.LastIndexOf('/') + 1);
		viewType = uiType;
		viewMode = uiMode;
	}

	public override string ToString(){
		return string.Format("path : {0} name : {1}", path, name);
	}

	/// <summary>
	/// 用户测试界面
	/// </summary>
	public static readonly UIType UserTestView = new UIType("UI/UserTestView", Sysdefine.ViewType.NORMAL, Sysdefine.ViewMode.NORMAL);
}