﻿public class BaseContext 
{

	public UIType viewType { get; private set; }

	public BaseContext(UIType uiType)
	{
		viewType = uiType;
	}
}