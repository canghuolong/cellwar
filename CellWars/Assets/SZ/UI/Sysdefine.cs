﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sysdefine  {
	//UI窗体（位置）类型
	public enum ViewType
	{
		/// <summary>
		/// 普通窗体
		/// </summary>
		NORMAL,   
		/// <summary>
		/// 固定窗体  
		/// </summary>
		FIXED,
		/// <summary>
		/// 弹出窗体
		/// </summary>
		POPUP
	}

	//UI窗体的显示类型
	public enum ViewMode
	{
		/// <summary>
		/// 普通
		/// </summary>
		NORMAL,
		/// <summary>
		/// 反向切换
		/// </summary>
		REVERSECHANGE,
		/// <summary>
		/// 隐藏其他
		/// </summary>
		HIDEOTHER
	}

}
