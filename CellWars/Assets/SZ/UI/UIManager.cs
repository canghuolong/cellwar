﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager  {
	const string UIMANAGERPATH = "UI/UIManager";
	const string NORMALROOT = "normalRoot";
	const string FIXEDROOT = "fixedRoot";
	const string POPUPROOT = "popupRoot";
	const string UILAYER = "UI";
	const int DEFAULTUIDEPTH = 1;
	static UIManager instance;
	public static UIManager getInstance {
		get {
			if(instance == null) {
				instance = new UIManager();
			}
			return instance;
		}
	}
	GameObject uiManager;
	public Camera uiCamera;
	Transform normalRoot;
	Transform fixedRoot;
	Transform popupRoot;

	Dictionary<UIType, ViewBase> viewDic = new Dictionary<UIType, ViewBase>();

	UIManager() {
		uiManager = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>(UIMANAGERPATH));
		uiManager.name = UIMANAGERPATH;
		uiCamera = uiManager.GetComponentInChildren<Camera>();
		uiCamera.depth = DEFAULTUIDEPTH;
		normalRoot = uiManager.transform.FindDeepChild(NORMALROOT);
		fixedRoot = uiManager.transform.FindDeepChild(FIXEDROOT);
		popupRoot = uiManager.transform.FindDeepChild(POPUPROOT);
		GameObject.DontDestroyOnLoad(uiManager);
		SZDebug.Log("uimanager init", SZDebug.Color.GREEN);
	}

	public ViewBase GetView(UIType viewName) {
		if(viewDic.ContainsKey(viewName) ) {
			return viewDic[viewName];
		}
		SZDebug.Log("there is no view:" + viewName);
		return null;
	}

	public void CreateView(UIType uiType, BaseContext context = null) {
		SZDebug.Log(uiType.ToString() + ":" + uiType.viewType, SZDebug.Color.YELLOW);
		if(!viewDic.ContainsKey(uiType) || viewDic[uiType] != null) {
			GameObject go = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>(uiType.path));
			go.name = uiType.name;
			go.ChangeLayer(LayerMask.NameToLayer(UILAYER));
			ViewBase viewBase = go.GetComponent<ViewBase>();
			viewDic.AddOrReplace(uiType, viewBase);
			switch (uiType.viewType) {
				case Sysdefine.ViewType.NORMAL:
					go.transform.SetParent(normalRoot);
					break;
				case Sysdefine.ViewType.FIXED:
					go.transform.SetParent(fixedRoot);
					break;
				case Sysdefine.ViewType.POPUP:
					go.transform.SetParent(popupRoot);
					break;
			}
			go.transform.localPosition = Vector3.zero;
			go.transform.localScale = Vector3.one;
			viewBase.Create(context);
		}
	}

	public void CloseView(UIType view) {
		if(viewDic.ContainsKey(view)) {
			viewDic[view].Close();
			GameObject.Destroy(viewDic[view].gameObject);
			viewDic.Remove(view);
		} else {
			SZDebug.Log("there is no view:" + view);
			return;
		}
	}

}
