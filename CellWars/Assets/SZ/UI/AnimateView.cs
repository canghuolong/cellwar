﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AnimateView : ViewBase {
	public override void Create(BaseContext context)
	{
		base.Create(context);
	}

	public override void Close(BaseContext context = null)
	{
		base.Close(context);
	}

	public override void OnRegister()
	{
		base.OnRegister();
	}

	public override void OnUnRegister()
	{
		base.OnUnRegister();
	}

	public abstract void AnimationEnd(System.Action callBack);
	public abstract void AnimationBegin(System.Action callBack);
}

