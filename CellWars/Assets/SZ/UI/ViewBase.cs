﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ViewBase : MonoBehaviour, IViewBase
{
	public virtual void Create(BaseContext context)	{
	}

	public virtual void Close(BaseContext context = null){
		
	}

	public virtual void OnRegister(){
		
	}

	public virtual void OnUnRegister(){
		
	}

}


public interface IViewBase{
		
	void OnRegister();

	void OnUnRegister();

	void Create(BaseContext context);

	void Close(BaseContext context);
}
